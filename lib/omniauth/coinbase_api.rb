module CoinbaseAPI

  class Client 
   BASE_URI = 'https://api.coinbase.com/v1'
   def get_balance
     at = access_token 
     error=nil
   begin
     @json = at.get("api/v1/users/me/balance").parsed  
   end
   end
    
  def initialize(apikey, apisecret, user_credentials)
    @apikey = apikey
    @apisecret = apisecret
    @site = 'https://coinbase.com'
    @user_credentials = user_credentials
    access_token
  end
  
  def client
    @client ||= OAuth2::Client.new(@apikey, @apisecret, :site => @site)
  end
  
  def refresh!
    @token.refresh!
  end

  def access_token  
    @token ||= OAuth2::AccessToken.new(client, @user_credentials[:access_token],{:refresh_token => @user_credentials[:refresh_token],
      :expires_in => @user_credentials[:expires_at]})  
  end
  
  
  def balance
    addr = BASE_URI + '/account/balance'
    @json = @token.get(addr).parsed["amount"]
  end
  
  def receive_address
    
  end
  
  def send_money(to_address, amount, message, options)
     addr = BASE_URI + '/transactions/send_money'
     
   body ={  
     "transaction"  => {"to" => to_address, "amount" => amount, "notes" => message }  
   }
   body["transaction"].merge!(options)
   
   opts = {}
   opts[:body] = body.to_json
  @json = @token.post(addr,opts ).parsed
  @json.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo} #convert keys to symbols
  end
    
  end
  
end