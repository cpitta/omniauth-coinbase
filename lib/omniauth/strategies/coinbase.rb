require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Coinbase < OmniAuth::Strategies::OAuth2
      SCOPES = 'balance addresses buy send:bypass_2fa transactions transfers user send'
      option :name, 'coinbase'
      option :client_options, {
          :site => 'https://coinbase.com',
          :proxy => ENV['http_proxy'] ? URI(ENV['http_proxy']) : nil
      }

      uid { raw_info['id'] }

      info do
        {
            :id => raw_info['id'],
            :name => raw_info['name'],
            :email => raw_info['email'],
            :balance =>  raw_info['balance'].present? ? raw_info['balance']['amount'] : 0,
            :buy_limit => raw_info['buy_limit'].present? ? raw_info['buy_limit']['amount'] : 0  ,
            :buy_limit_currency=>  raw_info['buy_limit'].present? ? raw_info['buy_limit']['currency'] : nil  ,
            :sell_limit => raw_info['sell_limit'].present? ? raw_info['sell_limit']['amount'] : 0,
            :sell_limit_currency => raw_info['sell_limit'].present? ? raw_info['sell_limit']['currency'] : nil,
            :native_currency => raw_info['native_currency'],
            :time_zone => raw_info['time_zone']
        }
      end

      extra do
        { :raw_info => raw_info }
      end

      def raw_info
        @raw_info ||= MultiJson.load(access_token.get('/api/v1/users').body)['users'][0]['user']
      rescue ::Errno::ETIMEDOUT
        raise ::Timeout::Error
      end
      def authorize_params
        super.tap do |params|
          params[:scope] ||= SCOPES 
          params["meta[send_limit_amount]"]= '500'
          params["meta[send_limit_period]"]= 'daily'
        end
      end

    end
  end
end